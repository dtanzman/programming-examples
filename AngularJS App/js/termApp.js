angular.module("mainApp", ['ngRoute', 'ngAnimate', 'ngSanitize', 'ui.bootstrap', 'ui'])

    .config(function($routeProvider) {
        $routeProvider
            .when('/search', {
                templateUrl: 'pages/fullSearch.template.html',
                controller: 'bookSearchCtrl',
                activetab: 'search'
            })
            .when('/toReadList', {
                templateUrl: 'pages/readingList.template.html',
                controller: 'readingListCtrl',
                activetab: 'toReadList'
            })
            .when('/archiveList', {
                templateUrl: 'pages/readingList.template.html',
                controller: 'archiveListCtrl',
                activetab: 'archiveList'
            })
            .otherwise({
                // default route page
                redirectTo: '/toReadList'
            });
    })

    .controller('tabCtrl', function($scope, $route) {
        // expose $route to controller
        $scope.$route = $route;
    })

    .controller('bookSearchCtrl', function($scope, $http) {
        $scope.myData;
        $scope.queryInputPlaceholder = "Title, Author, ISBN, ...";
        $scope.addButtonText = "Add";
        $scope.addButtonClass = "btn btn-warning";
        $scope.postAddButtonClass = "btn btn-success";
        $scope.books = new Array();

        // pagination variables
        $scope.pagination = {
            maxSize: 5,
            itemsPerPage: 10,
            totalItems: null,
            currentPage: 1
        }

        // check if any books already saved
        if (window.localStorage.getItem("tanzman_books") != null) {
            // load from local storage
            var data = window.localStorage.getItem("tanzman_books");
            $scope.books = JSON.parse(data);
            console.log($scope.books);
        }

        // Next Page
        $scope.nextPage = function() {
            // enable loading message
            $scope.dataLoading = true;

            console.log("Current page:", $scope.pagination.currentPage);
            //console.log("Selected page:", page);

            var startIndex = ($scope.pagination.currentPage - 1) * $scope.pagination.itemsPerPage; //ex p2 starts at item 10 ((2-1)*10)

            // search for new pages books
            console.log("New Page start at: " + $scope.pagination.currentPage + " Query:" + $scope.queryInput + "startIndex:" + startIndex);
            $scope.findBooks($scope.queryInput, startIndex);

            // scroll to top
            document.body.scrollTop = 0; // For Chrome, Safari and Opera 
            document.documentElement.scrollTop = 0; // For IE and Firefox
        }

        $scope.setPage = function(pageNo) {
            $scope.pagination.currentPage = pageNo;
            console.log("Set page: " + $scope.pagination.currentPage);
        };

        // Find Books
        $scope.findBooks = function(input, startIndex) {
            if (startIndex <= 0) {
                console.log("reset pagination")
                $scope.setPage(1);
                startIndex = 0;
            }
            $scope.queryInput = input;

            // start data loading message
            $scope.dataLoading = true;

            // Google Book API requires GET request
            $http.get("https://www.googleapis.com/books/v1/volumes?q=" + $scope.queryInput + "&startIndex=" + startIndex + "&maxResults=" + $scope.pagination.itemsPerPage)
                .then(function successCallback(response) {

                    // this callback will be called asynchronously
                    // when the response is available

                    // JSON data broken out by items
                    $scope.myData = response.data.items;

                    // limit pagenation to 100 items
                    if (response.data.totalItems <= 100) {
                        $scope.pagination.totalItems = totalItems;
                    } else {
                        $scope.pagination.totalItems = 100;
                    }
                    // logs object array for easy reference (Remove before Production)
                    console.log(response.data.totalItems);
                    console.log($scope.myData);

                }, function errorCallback(response) {

                    // called asynchronously if an error occurs
                    // or server returns response with an error status.

                    // log errors
                    console.log("Error: ", response);
                }).finally(function() {
                    $scope.dataLoading = false;
                });
        }

        // called from "check-book-exists-directive" in fullSearch template
        $scope.checkBookExists = function() {
            console.log("Running checkBookExists");
            // start with newest saved items
            for (var x = $scope.books.length - 1; x >= 0; x--) {
                var savedBookISBN = $scope.books[x].isbn;
                // start with top of search items
                for (var y = 0; y < $scope.myData.length; y++) {
                    var searchBookISBN;
                    if ($scope.myData[y].volumeInfo.industryIdentifiers != null){
                      searchBookISBN = $scope.myData[y].volumeInfo.industryIdentifiers[0].identifier;
                    } else {
                      // drop that searched book
                      $scope.myData.splice(y, 1);
                    }
                    if (savedBookISBN === searchBookISBN) {
                        console.log("Already Saved ISBN:" + searchBookISBN);
                        // update button as submitted
                        $scope.markButtonSubmitted(y, true);
                    };
                };
            };
        }

        $scope.addBook = function(object, index) {
            // target button
            var thisButton = document.getElementById("addButton" + index);
            var selectedBook = object;

            // split pre-post button press responses
            if (thisButton.className == $scope.postAddButtonClass) {
                // Request Undo
                console.log("Remove book from index:" + index);
                $scope.removeBook(selectedBook);
                // update button to original
                $scope.markButtonSubmitted(index, false);
            } else {
                // Request Add
                console.log("Added book from index:" + index);
                var keyData = {
                    "isbn": selectedBook.volumeInfo.industryIdentifiers[0].identifier,
                    "thumbnail": selectedBook.volumeInfo.imageLinks.thumbnail,
                    "title": selectedBook.volumeInfo.title,
                    "subtitle": selectedBook.volumeInfo.subtitle,
                    "authors": selectedBook.volumeInfo.authors,
                    "publishedDate": selectedBook.volumeInfo.publishedDate,
                    "textSnippet": selectedBook.searchInfo.textSnippet,
                    "description": selectedBook.volumeInfo.description
                };

                $scope.books.push(keyData);
                // update button as submitted
                $scope.markButtonSubmitted(index, true);
            }
            // save changes
            // add books to local storage
            console.log("saved new books:",$scope.books);
            window.localStorage.setItem("tanzman_books", JSON.stringify($scope.books));
        }

        $scope.removeBook = function(object) {
            //locate in book index
            //var books = window.localStorage.getItem("tanzman_books");
            var bookIndex = $scope.findBookIndex(object);
            if (bookIndex[0]) {
                // book exists
                console.log("Deleted Book from toRead index:" + bookIndex[1]);
                $scope.books.splice(bookIndex[1], 1);
            } else {
                console.log("Error: Book not found in toRead list");
            }
        }

        $scope.findBookIndex = function(object) {
            var thisBookISBN = object.volumeInfo.industryIdentifiers[0].identifier;
            var bookExists = false;
            var bookIndex;

            // start with top of toRead list
            for (var y = 0; y < $scope.books.length; y++) {
                var toReadBookISBN = $scope.books[y].isbn;

                if (thisBookISBN === toReadBookISBN) {
                    console.log("Already Saved ISBN:" + thisBookISBN);

                    bookExists = true;
                    bookIndex = y;
                }
            };
            // return array of parameters
            return [bookExists, bookIndex];
        }

        $scope.markButtonSubmitted = function(index, boolean) {
            var thisButton = document.getElementById("addButton" + index);
            var preAddClass = $scope.addButtonClass;
            var preAddText = $scope.addButtonText;
            var postAddClass = $scope.postAddButtonClass;

            if (boolean) {
                // Need to Update
                // update with new classes & text
                thisButton.className = postAddClass;
                thisButton.innerHTML = "&#10004;";
            } else {
                // Already Updated
                // reintroduce old classes & text
                thisButton.className = preAddClass;
                thisButton.innerHTML = preAddText;
            }
        }

    })

    .controller('readingListCtrl', function($scope) {
        $scope.pageTitle = "Reading List";
        $scope.queryInputPlaceholder = "Title, Author, ...";
        $scope.addButtonText = "Archive";
        $scope.addButtonClass = "btn btn-warning";
        $scope.postAddButtonClass = "btn btn-success";
        $scope.toRead = new Array();
        $scope.archive = new Array();
        $scope.toReadList = true;
        $scope.queryInput = new Object();

        $scope.orderingOptions = [
            { text: 'Manual', value: '' },
            { text: 'Title', value: 'title' },
            { text: 'Author', value: 'authors[0]' },
            { text: 'Published Date', value: 'publishedDate' }
        ];
        $scope.orderingProperties = { orderProp: ''}; // default sort property

        // check if any books already saved
        if (window.localStorage.getItem("tanzman_books") != null) {
            // load from local storage
            var data = window.localStorage.getItem("tanzman_books");
            $scope.toRead = JSON.parse(data);
            $scope.books = $scope.toRead;
            console.log($scope.toRead);
        }

        // flag if no books
        $scope.noBooks = ($scope.books == null || $scope.books.length == 0 ? true : false); //using conditional (ternary) operator

        // check if archive exists
        if (window.localStorage.getItem("tanzman_archive") != null) {
            // load from local storage
            var data = window.localStorage.getItem("tanzman_archive");
            $scope.archive = JSON.parse(data);
        }

        // check if any options saved
        if (window.localStorage.getItem("tanzman_options") != null) {
            // load from local storage
            var data = window.localStorage.getItem("tanzman_options");
            $scope.orderingProperties.orderProp = JSON.parse(data);
            console.log("Loaded prior options", $scope.orderProp);
        }

        // save options updates
        $scope.optionValueChange = function(newOption) {
            console.log("Options updated:", newOption);
            // save new orderProp
            window.localStorage.setItem("tanzman_options", JSON.stringify(newOption));
        }

        // save drag and drop ordering changes
        $scope.$watchCollection('books', function(newBooks, oldBooks) {
            console.log("Books Array Changed:", newBooks);
            // save new array order
            
            if(newBooks != null){
              window.localStorage.setItem("tanzman_books", JSON.stringify(newBooks));
            }
        });

        $scope.addBook = function(object) {
            // target button
            //var thisButton = document.getElementById("addButton"+index);
            var selectedBook = object;
            var bookExists = $scope.checkBookExists(selectedBook);

            // check if book exists
            console.log("Does Book Exist:", bookExists[0]);

            // add book to archive
            if (bookExists[0]) {
                // replace archive book
                var oldIndex = bookExists[1];
                console.log("removed archive book Index:", oldIndex);
                // remove old book
                $scope.archive.splice(oldIndex, 1);
                // add archive book
                console.log("Re-Archived book:" + object.title);
                $scope.archive.push(selectedBook);
            } else {
                // add archive book
                console.log("Archived book:" + object.title);
                $scope.archive.push(selectedBook);
            }

            // remove from toRead
            $scope.removeBook(selectedBook);

            // save changes
            window.localStorage.setItem("tanzman_books", JSON.stringify($scope.books));
            window.localStorage.setItem("tanzman_archive", JSON.stringify($scope.archive));
        }

        $scope.checkBookExists = function(object) {
            var thisBookISBN = object.isbn;
            var bookExists = false;
            var bookIndex;

            // start with top of archive items
            for (var y = 0; y < $scope.archive.length; y++) {
                var archiveBookISBN = $scope.archive[y].isbn;

                if (thisBookISBN === archiveBookISBN) {
                    console.log("Already Saved ISBN:" + thisBookISBN);

                    bookExists = true;
                    bookIndex = y;
                }
            };
            // return array of parameters
            return [bookExists, bookIndex];
        }

        $scope.removeBook = function(object) {
            //locate in book index
            var bookIndex = $scope.books.indexOf(object);
            if (bookIndex == -1) {
                console.log("Error: Couldn't find book index to remove book");
            } else {
                console.log("Removed toRead book from index:" + bookIndex);
                $scope.books.splice(bookIndex, 1);
            }
        }
    })

    .controller('archiveListCtrl', function($scope) {
        $scope.pageTitle = "Archive List";
        $scope.queryInputPlaceholder = "Title, Author, ...";
        $scope.addButtonText = "Un-Archive";
        $scope.addButtonClass = "btn btn-warning";
        $scope.postAddButtonClass = "btn btn-success";
        $scope.toRead = new Array();
        $scope.archive = new Array();
        $scope.archiveList = true;
        $scope.queryInput = new Object();

        $scope.orderingOptions = [
            { text: 'Manual', value: '' },
            { text: 'Title', value: 'title' },
            { text: 'Author', value: 'authors[0]' },
            { text: 'Published Date', value: 'publishedDate' }
        ];
        $scope.orderingProperties = { orderProp: ''}; // default sort property

        // check if archive already saved
        if (window.localStorage.getItem("tanzman_archive") != null) {
            // load from local storage
            var data = window.localStorage.getItem("tanzman_archive");
            $scope.archive = JSON.parse(data);
            $scope.books = $scope.archive;
            console.log($scope.archive);
        }

        // flag if no books
        $scope.noBooks = ($scope.books == null || $scope.books.length == 0 ? true : false); //using conditional (ternary) operator

        // check if any books already saved
        if (window.localStorage.getItem("tanzman_books") != null) {
            // load from local storage
            var data = window.localStorage.getItem("tanzman_books");
            $scope.toRead = JSON.parse(data);
        }

        // check if any options saved
        if (window.localStorage.getItem("tanzman_options") != null) {
            // load from local storage
            var data = window.localStorage.getItem("tanzman_options");
            $scope.orderingProperties.orderProp = JSON.parse(data);
            console.log("Loaded prior options", $scope.orderProp);
        }

        // save options updates
        $scope.optionValueChange = function(newOption) {
            console.log("Options updated:", newOption);
            // save new orderProp
            window.localStorage.setItem("tanzman_options", JSON.stringify(newOption));
        }

        // save drag and drop ordering changes
        $scope.$watchCollection('books', function(newBooks, oldBooks) {
            console.log("Archive Book Array Changed:", newBooks);
            // save new array order
            if(newBooks != null){
              window.localStorage.setItem("tanzman_archive", JSON.stringify(newBooks));
            }
        });

        // save options updates
        $scope.$watch('orderProp', function(newOption, oldOption) {
            console.log("Options updated:", newOption);
            if(newOption != null){
                // save new array order
                window.localStorage.setItem("tanzman_options", JSON.stringify(newOption));
              }
        });

        $scope.addBook = function(object) {
            var selectedBook = object;
            var bookExists = $scope.checkBookExists(selectedBook);

            // check if book exists
            console.log("Does Book Exist:", bookExists[0]);

            // add archive to toRead
            if (bookExists[0]) {
                // replace toRead book
                var oldIndex = bookExists[1];
                console.log("removed archive book Index:", oldIndex);
                // remove old book
                $scope.toRead.splice(oldIndex, 1);
                // add toRead book
                console.log("Re-Archived book:" + object.title);
                $scope.toRead.push(selectedBook);
            } else {
                // add toRead book
                console.log("Archived book:" + object.title);
                $scope.toRead.push(selectedBook);
            }

            // remove from toRead
            $scope.removeBook(selectedBook);

            // save changes
            window.localStorage.setItem("tanzman_archive", JSON.stringify($scope.books));
            window.localStorage.setItem("tanzman_books", JSON.stringify($scope.toRead));
        }

        $scope.checkBookExists = function(object) {
            var thisBookISBN = object.isbn;
            var bookExists = false;
            var bookIndex;

            // start with top of archive items
            for (var y = 0; y < $scope.toRead.length; y++) {
                var toReadBookISBN = $scope.toRead[y].isbn;

                if (thisBookISBN === toReadBookISBN) {
                    console.log("Already Saved ISBN:" + thisBookISBN);

                    bookExists = true;
                    bookIndex = y;
                }
            };
            // return array of parameters
            return [bookExists, bookIndex];
        }

        $scope.removeBook = function(object) {
            //locate in book index
            var bookIndex = $scope.books.indexOf(object);
            $scope.books.splice(bookIndex, 1);
        }
    })

    /* Not Required since built into the Form group functionality
    // trigger passed function on enter
    .directive('myEnter', function() {
        return function(scope, element, attrs) {
            element.bind("keydown keypress", function(event) {
                if (event.which === 13) {
                    scope.$apply(function() {
                        scope.$eval(attrs.myEnter);
                    });

                    event.preventDefault();
                }
            });
        };
    })*/

    // run function to check which books already saved
    .directive('checkBookExistsDirective', function() {
        return function(scope, element, attrs) {
            // runs when page finished loading
            scope.$watch('$last', function(v) {
                if (v) scope.checkBookExists();
            });
        }
    })

    // make html input trusted
    .filter('html', function($sce) {
        return function(input) {
            return $sce.trustAsHtml(input);
        }
    })