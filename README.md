## Programming Examples

 - *Android TicTacToe App* **`Java`** **`XML`** **`Android Studio's`**
 	- Demonstrates a dynamic 2-player Android Tic-Tac-Toe App where one can modify the board size, timer, and max rounds
 	- Created in Android Studio's using Java and XML
 	- Preferences auto save to devices memory

 - *AngularJS App* **`JS`** **`AngularJS`** **`JQuery`** **`HTML`** **`CSS`** **`BootStrap`**
  	- Demonstrates a AngularJS web application that allows the user to track the books one wants to read
  	- This app utilizes Google Books API for its source data and then saves that data to the browsers localStorage
  	- See Example specific README.docx for more info

 - *DOM Manipulation* **`HTML`** **`CSS`** **`JS`** **`JQuery`**
 	- Example of manipulating HTML DOM to move/modify images 

 - *Ionic - BookE App (external)* **`HTML`** **`CSS`** **`BootStrap`** **`Angular 4`** **`Ionic 3`** **`TypeScript`** **`Cordova`**
	- Demonstrates a multi-platform Ionic mobile application that allows the user to track the books one wants to read
	- This app utilizes Google Books API for its source data then saves that data to the mobile devices internal memory
	- See https://bitbucket.org/dtanzman/ionic-booke-app/ for source files

 - *NodeJS App & REST API* **`JS`** **`Node.js`** **`Express.js`** **`HTML`** **`BootStrap`** **`Handlebars`** **`MongoDB`**
 	- Cross-platform Node.js task planner that doubles as a REST API for outputs in JSON and XML
 	- Utilized Passport for login, MongoDB for DB storage, Express.js for routing, and Handlebars plus BootStrap for backend display logic
 	- See Example specific README.txt for more info

 - *PHP App & REST API* **`PHP`** **`HTML`** **`CSS`** **`SQL`**
  	- Demonstrates a multi page PHP application that adds, removes, and displays items stored in a SQL DB
  	- See rest.php for example REST API for outputs in JSON and XML

 - *Sample Dynamic Website* **`HTML`** **`CSS`** **`JS`** **`JQuery`**
 	- Example of Dynamic HTML Website 
 	- Created own dynamic structure for page resizing and printing