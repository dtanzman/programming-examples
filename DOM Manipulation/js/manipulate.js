/* 
    Created on : Jun 8, 2016, 5:26:40 PM
    Author     : David Tanzman
    JavaScript/jQuery to Manipulate Images utilizing the DOM 
*/

// Waits till window loads before loads JavaScript
window.onload = function () {
    // add wrappedElement Class when window loads
    $("img").addClass("wrappedElement"); // adds black border and margin
    
    //Load Buttons when window loads
    $("#hideButton").click(function(){
        // hide all images
        $("img").hide();
    });
    
    $("#evenButton").click(function(){
        // show even images
        $("img:even").show("slow");
    });
    
    $("#oddButton").click(function(){
        // show odd images
        $("img:odd").show("slow");
    });
    
    $("#rightButton").click(function(){
        // move last img to beginning
        $("img:last").detach().prependTo("#images");
    });
    
    $("#leftButton").click(function(){
        // move last img to beginning
        $("img:first").detach().appendTo("#images");
    });

    // Format Buttons using jQueryUI   
    $("button").button().click(function(event) {
        event.preventDefault();
    });
    
    // When click on image toggles between regular image and inverted image filter
    $("img").click(function(){
        $(this).toggleClass("invert");
    });

    $("#zoomEvenButton").click(function(){
        $("img:even").toggleClass("scale");
    });
    
    $("#zoomOddButton").click(function(){
        $("img:odd").toggleClass("scale");
    });
}; 






