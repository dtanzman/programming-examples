<?php
    // Contains shared variables
    require('database.php');
?>

<!DOCTYPE html>
<html>
<!-- head section -->
<head>
    <title>Assignment 5</title>
    <link rel="stylesheet" type="text/css" href="main.css" />
</head>
<!-- body section -->
<body class="lists">
    <header><h1>Course Manager</h1></header>
        <main>
            <h1>Course List</h1>
            <table>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                </tr>
                
                <?php foreach ($courses as $course) : ?>
                <tr>
                    <td><?php echo $course['courseID']; ?></td>
                    <td><?php echo $course['courseName']; ?></td>
                </tr>
                <?php endforeach; ?>
                
            </table>
            <p>
            <h2>Add Course</h2>
            
            <!-- add code for the form here -->
            <form action="add_course.php" method="post">
                <label>Course ID:</label>
                <input type="text" name="cid"><br>
                <label>Course Name:</label>
                <input type="text" name="cname"><br>
                <p>
                    <label>&nbsp;</label>
                    <input type="submit" value="Add Course"><br>
                </p>
            </form>
            <br>
            <p><a href="index.php">List Students</a></p>
        </main>
        <footer>
            <p>&copy; <?php echo date("Y"); ?> David Tanzman.</p>
        </footer>
    </body>
</html>