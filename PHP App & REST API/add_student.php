<?php
// Get the student data
$course_id = filter_input(INPUT_POST, 'cid');
$fname = filter_input(INPUT_POST, 'fname');
$lname = filter_input(INPUT_POST, 'lname');
$email = filter_input(INPUT_POST, 'email');

// Validate inputs
if ($course_id == null || $fname == null || $lname == null || $email == null) {
    $error = "Invalid student data. Populate all fields and try again.";
    include('./errors/error.php');
} else {
    require_once('database.php');

    // Add student to the database  
    $query = 'INSERT INTO dt_students
                 (courseID, firstName, lastName, email)
              VALUES
                 (:course_id, :fname, :lname, :email)';
    $statement = $db->prepare($query);
    $statement->bindValue(':course_id', $course_id);
    $statement->bindValue(':fname', $fname);
    $statement->bindValue(':lname', $lname);
    $statement->bindValue(':email', $email);
    $statement->execute();
    $statement->closeCursor();

    
    // Display home page content
    include('index.php');

    /* Alternative Solution if didn't modify home pages links to ./index.php?course_id=
    // Redirect to Course List page (http://php.net/manual/en/function.header.php)
    $host  = $_SERVER['HTTP_HOST'];
    $uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
    header("Location: http://$host$uri");
    exit;
    */
}
?>