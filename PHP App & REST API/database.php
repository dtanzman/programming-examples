<?php
    //Replace with your own MySQL credentials for working example
    $dsn = 'mysql:host=localhost;dbname=sqldb';
    $username = 'yourUsername';
    $password = 'yourPassword';

    try {
        $db = new PDO($dsn, $username, $password);
    } catch (PDOException $e) {
        $error_message = $e->getMessage();
        include('./errors/database_error.php');
        exit();
    }
?>
<?php
    /*** Store Shared DB Variables ***/  
    // Get all courses
    $queryAllCourses = 'SELECT * FROM dt_courses
                        ORDER BY courseID';
    $statement = $db->prepare($queryAllCourses);
    $statement->execute();
    // Fetch all courses - note need to overide default (FETCH_BOTH) as that causes duplicate data
    $courses = $statement->fetchAll(PDO::FETCH_ASSOC); 
    $statement->closeCursor();

    // Set initial courseID & courseName to first one if not already defined (just opened app)
    if (!isset($course_id)) {
        $course_id = filter_input(INPUT_GET, 'course_id');
        if ($course_id == NULL || $course_id == FALSE) {
            $course_id = $courses[0]['courseID'];
        }
    }
    // Get name for selected course
    $queryCourse = 'SELECT * FROM dt_courses
                    WHERE courseID = :course_id';
    $statement1 = $db->prepare($queryCourse);
    $statement1->bindValue(':course_id', $course_id);
    $statement1->execute();
    $course = $statement1->fetch(PDO::FETCH_ASSOC);
    $course_name = $course['courseName']; // save to variable
    $statement1->closeCursor();

    // Get all students for selected course
    $queryStudents = 'SELECT * FROM dt_students
                      WHERE courseID = :course_id
                      ORDER BY studentID';
    $statement3 = $db->prepare($queryStudents);
    $statement3->bindValue(':course_id', $course_id);
    $statement3->execute();
    $students = $statement3->fetchAll(PDO::FETCH_ASSOC); // save to variable
    $statement3->closeCursor();
?>