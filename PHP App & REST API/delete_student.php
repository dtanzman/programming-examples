<?php
require_once('database.php');

// Set IDs
$student_id = filter_input(INPUT_POST, 'student_id');
$course_id = filter_input(INPUT_POST, 'course_id'); //keeps index.php on current course_id

// Delete the student from the database
if ($student_id != null) {
    $query = 'DELETE FROM dt_students
              WHERE studentID = :student_id';
    $statement = $db->prepare($query);
    $statement->bindValue(':student_id', $student_id);
    $success = $statement->execute();
    $statement->closeCursor();    
}

// Display the Student List page
include('index.php');