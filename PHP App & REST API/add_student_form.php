<?php
    // Contains shared variables
    require('database.php');
?>
<!DOCTYPE html>
<html>

<!-- the head section -->
<head>
    <title>Assignment 5</title>
    <link rel="stylesheet" type="text/css" href="main.css">
</head>

<!-- the body section -->
<body class="lists">
    <header><h1>Course Manager</h1></header>

    <main>
        <h1>Add Student</h1>
        <form action="add_student.php" method="post">

            <label>Course:</label>
            <select name="cid">
            <?php foreach ($courses as $course) : ?>
                <option value="<?php echo $course['courseID']; ?>">
                    <?php echo $course['courseID'].' - '.$course['courseName']; ?>
                </option>
            <?php endforeach; ?>
            </select><br>

            <label>First Name:</label>
            <input type="text" name="fname"><br>

            <label>Last Name:</label>
            <input type="text" name="lname"><br>

            <label>Email:</label>
            <input type="email" name="email"><br>

            <label>&nbsp;</label>
            <input type="submit" value="Add Student"><br>
        </form>
        <p><a href="index.php">View Student List</a></p>
    </main>

    <footer>
        <p>&copy; <?php echo date("Y"); ?> David Tanzman.</p>
    </footer>
</body>
</html>