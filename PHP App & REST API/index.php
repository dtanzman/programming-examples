<!-- Get Data -->
<?php
	// Contains shared variables
	require('database.php');
?>

<!-- Display HTML -->
<!DOCTYPE html>
<html>
	<!-- head section -->
	<head>
    <title>PHP Example</title>
   	<link rel="stylesheet" type="text/css" href="main.css" />
	</head> 

	<!-- body section -->
	<body>
		<header><h1>Course Manager</h1></header>
		<main>
			<h1 class="center">Student List</h1>
			<aside>
				<!-- display a list of courses -->
				<h2>Courses</h2>
				<nav>
					<ul>
						<?php foreach ($courses as $course) : ?>
							<li>
								<a href="./index.php?course_id=<?php echo $course['courseID']; ?>">
									<?php echo $course['courseID']; ?>
								</a>
							</li>
						<?php endforeach; ?>
					</ul>
				</nav>
			</aside>
			<section>
				<!-- display a table of students -->
				<h2><?php echo $course_id.' - '.$course_name; ?></h2>
				<table>
					<tr>
						<th>First Name</th>
						<th>Last Name</th>
						<th>Email</th>
						<th>&nbsp;</th>
					</tr>
					<?php foreach ($students as $student) : ?>
						<tr>
							<td><?php echo $student['firstName']; ?></td>
							<td><?php echo $student['lastName']; ?></td>
							<td><?php echo $student['email']; ?></td>
							<td>
								<form action="delete_student.php" method="post">
									<input type="hidden" name="student_id"
									value="<?php echo $student['studentID']; ?>">
									<input type="hidden" name="course_id"
									value="<?php echo $student['courseID']; ?>">
									<input type="submit" value="Delete">
								</form>
							</td>
						</tr>
					<?php endforeach; ?>
				</table>
				<p><a href="add_student_form.php">Add Student</a></p>
				<p><a href="course_list.php">List Courses</a></p>
			</section>
		</main>

		<footer>
			<p>&copy; <?php echo date("Y"); ?> David Tanzman.</p>
		</footer>
	</body>
</html>