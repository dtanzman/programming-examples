<?php
	// Contains shared db variables
	require('database.php');
?>
<?php

	// Get format and action inputs from url
	$format = strtolower(filter_input(INPUT_GET, 'format')); 
	$action = strtolower(filter_input(INPUT_GET, 'action'));
	$courseID = strtolower(filter_input(INPUT_GET, 'course'));

	// verify have minimum inputs
	if ($format != null && $action != null) {

			// must happend before require database.php since overiding a variable there
			if($courseID != null){
				// set db variable for course ID
				$course_id = $courseID;
			}

			// Contains shared db queries and output variables 
			require('database.php');

			//db variables (returned from database.php) (listed for convenience)
			$courses; 
			$course_id;
			$course_name;
			$students;

			// Switch between format types
			switch ($format) {//force format to lower case
				case 'xml':
					// Set header content-type to XML
					// Header must be before any outputs
					header("Content-type: text/xml");

					//determine xml output based on action
					if($action == 'courses'){
						// convert courses array to XML
						echo xmlCourses($courses);
					} elseif ($action == 'students') {
						// verify have courseID
						if($courseID != null){
							// convert students array to XML
							echo xmlStudents($students);
						} else {
							//error message
							header("Content-type: text/html");//change back to html
							echo "<p>Please specify valid course id</p>";
						}
					} else {
						// error message
						header("Content-type: text/html");//change back to html
						echo "<p>Please specify action of 'courses' or 'students'</p>";
					}
					break;
				case 'json':
					// Set header content-type to JSON
					header('Content-type: application/json');

					//determine JSON output based on action
					if($action == 'courses'){
						// convert courses array to JSON
						echo json_encode($courses, JSON_PRETTY_PRINT);//puts proper breaks
					} elseif ($action == 'students') {
						// verify have courseID
						if($courseID != null){
							// convert students array to JSON
							echo json_encode($students, JSON_PRETTY_PRINT);//puts proper breaks
						} else {
							//error message
							header("Content-type: text/html");//change back to html
							echo "<p>Please specify valid course id</p>";
						}
					} else {
						// error message
						header("Content-type: text/html");//change back to html
						echo "<p>Please specify action of 'courses' or 'students'</p>";
					}
					break;
				default:
					echo "<p>Please select format type of JSON or XML</p>";
					break;
			}
	} else {
	    // error message
			echo "Please specify both the Format & Action";
	}
?>
<?php 
	// XML funcitons (to make clearer)
	function xmlCourses($courses){
		// Create new DOMDocument object and set options
		$xml = new DOMDocument('1.0');//set XML version
		$xml->preserveWhiteSpace = false;
		$xml->formatOutput = true;

		// Create & add root element
		$root = $xml->createElement('dt_courses');
		$root = $xml->appendChild($root);

		// Add elements to XML
		foreach($courses as $values) {
			// Create & add course element
			$course = $xml->createElement('course');
			$course = $root->appendChild($course);

			//iterate over each objects values
			foreach($values as $key => $value){
				//create elements
				$i = $xml->createElement($key, $value);
				//apend elements
				$course->appendChild($i);
			}
		}
		// Return output as XML
		return $xml->saveXML();
	}
	function xmlStudents($students){

		// Create new DOMDocument object and set options
		$xml = new DOMDocument('1.0');//set XML version
		$xml->preserveWhiteSpace = false;
		$xml->formatOutput = true;

		// Create & add root element
		$root = $xml->createElement('dt_students');
		$root = $xml->appendChild($root);

		// Add elements to XML
		foreach($students as $values) {
			// Create & add student element
			$student = $xml->createElement('student');
			$student = $root->appendChild($student);

			//iterate over each objects values
			foreach($values as $key => $value){
				//create elements
				$i = $xml->createElement($key, $value);
				//apend elements
				$student->appendChild($i);
			}
		}
		// Return output as XML
		return $xml->saveXML();
	}
?>
