<?php
// Get the course data
$cid = filter_input(INPUT_POST, 'cid');
$cname = filter_input(INPUT_POST, 'cname');

// Validate inputs
if ($cid == null || $cname == null) {
    $error = "Empty field. Check all fields and try again.";
    include('./errors/error.php');
} else {
    require_once('database.php');

    // Add the course to the database  
    $query = 'INSERT INTO dt_courses
              (courseID, courseName)
              VALUES
              (:cid, :cname)';
    $statement = $db->prepare($query);
    $statement->bindValue(':cid', $cid);
    $statement->bindValue(':cname', $cname);
    $statement->execute();
    $statement->closeCursor();

    // Display the Course List page
    include('course_list.php');
}
?>