package com.dtanzman.appdraft;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class WinnerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_winner);

        TextView mTitle = (TextView) findViewById(R.id.win_title);
        TextView mSubTitle = (TextView) findViewById(R.id.win_sub_title);

        // recover passed message
        Intent intent = getIntent();
        String titleMessage = intent.getStringExtra("titleMessageKey");
        String subTitleMessage = intent.getStringExtra("subTitleMessageKey");

        // update titles
        mTitle.setText(titleMessage);
        mSubTitle.setText(subTitleMessage);

    }

    // Called when user taps "Click To Play" button
    public void goToGame(View view) {
        Intent intent = new Intent(this, myMainActivity.class); // Set intent to move from this activity to other activity
        startActivity(intent); // Trigger intent
    }

    // Called when hit Settings button
    public void goToSettings(View view) {
        Intent intent = new Intent(this, PrefActivity.class); // Set intent to move from this activity to other activity
        startActivity(intent); // Trigger intent
    }

    // When leave this activity destroy it
    @Override
    protected void onStop() {
        super.onStop();
        finish(); // destroy activity
    }
}
