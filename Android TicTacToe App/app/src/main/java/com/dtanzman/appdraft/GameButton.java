package com.dtanzman.appdraft;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.CountDownTimer;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by dtanzman100 on 3/29/17.
 */

@SuppressLint("AppCompatCustomView")
public class GameButton extends ImageButton {
    // Variables
    private static final String TAG = "myMainActivity"; // Allows us to filter log messages in log output
    private int row;
    private int column;
    private String buttonID;

    public GameButton(Context context) {
        super(context);
    }

    public GameButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GameButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public GameButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected boolean onSetAlpha(int alpha) {
        return super.onSetAlpha(alpha);
    }

    @Override
    public CharSequence getAccessibilityClassName() {
        return super.getAccessibilityClassName();
    }


    /** Game Button methods */

    // Setters
    public void setButtonID(String buttonID) {
        this.buttonID = buttonID;
    }
    public void setRow(int row){
        this.row = row;
    }
    public void setColumn(int col){
        this.column = col;
    }

    // Getters
    public String getButtonID() {
        return buttonID;
    }
    public int getRow(){
        return this.row;
    }
    public int getColumn(){
        return this.column;
    }
}
