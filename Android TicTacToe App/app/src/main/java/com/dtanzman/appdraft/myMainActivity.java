package com.dtanzman.appdraft;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class myMainActivity extends AppCompatActivity {

    // Declare Variables
    private static final String TAG = "myMainActivity"; // Enables filtering log messages in log output
    TextView mTimerTitle;
    TextView mTimerOut;
    TextView mScoreOut;
    TextView mPlayerTurnOut;
    GameButton thisButton;
    char PlayerTurn = 'x';
    int scoreX = 0;
    int scoreO = 0;
    String gameScore = scoreX + ":" + scoreO;
    int thisGameRound = 1;
    GridLayout mGridLayout;

    // Setting Variables
    int prefGameRounds;
    int prefGridSize;
    boolean prefTimerEnabled = false;
    int prefStartTimerSec;

    // Timer Variables
    public Handler mHandler; // handler for Timer thread
    int timerSeconds;
    boolean timerPaused = false; // initially not paused

    // Leverage setting variables
    int[][] boardGrid; // initialize double int array, used for recovering imageButton images from state change
    int turnsLeft;

    // Initialize display metric
    DisplayMetrics displayMetrics = new DisplayMetrics();

    // Winner titles
    String titleMessage;
    String subTitleMessage;

    // Animation variables
    Animation animZoomIn;
    LinearLayout mTxtZoomIn;
    TextView mTxtZoomInTitle;
    TextView mTxtZoomInSubTitle;
    boolean animationActivated;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_page);

        toolbar(); // calls below method to setup toolbar

        Log.i(TAG, "onCreate");

        // initialize member variables so can manipulate later
        mTimerTitle = (TextView) findViewById(R.id.timerTitle);
        mTimerOut = (TextView) findViewById(R.id.timerOut);
        mScoreOut = (TextView) findViewById(R.id.scoreOut);
        mPlayerTurnOut = (TextView) findViewById(R.id.playerTurnOut);
        mGridLayout = (GridLayout) findViewById(R.id.gridLayout); //Target grid
        mTxtZoomIn = (LinearLayout) findViewById(R.id.txt_zoom_in); //Target animation linear grouping
        mTxtZoomInTitle = (TextView) findViewById(R.id.txt_zoom_in_title);
        mTxtZoomInSubTitle = (TextView) findViewById(R.id.txt_zoom_in_subtitle);
        animZoomIn = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.round_winner_animation); //target animation

        //Instantiate handler to run on UI Thread
        mHandler = new Handler();

        // calls below method to get preference variables
        restorePreferenceVariables();

        // log values of saved preference variables
        Log.i(TAG, "prefTimerEnabled=" + prefTimerEnabled);
        Log.i(TAG, "prefStartTimerSec=" + prefStartTimerSec);
        Log.i(TAG, "prefGameRounds=" + prefGameRounds);
        Log.i(TAG, "prefGridSize=" + prefGridSize);

        turnsLeft = prefGridSize * prefGridSize;

        // Run autoScale Grid logic and build dynamic Grid
        int pxCellSize = autoScaledGridCellSize(); // returns optimal cell size based on screen dimensions
        buildDynamicGrid(mGridLayout, pxCellSize, prefGridSize); // builds Grid

    }

    private void restorePreferenceVariables() {
        /** Get Variables from Saved Preferences */
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false); // initialize preferences default values
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this); // Get preference values (batch)

        // Recover specific pref values from batch and assign to variables
        prefGameRounds = Integer.parseInt(sharedPref.getString("rounds_prefs", ""));
        prefGridSize = Integer.parseInt(sharedPref.getString("grid_prefs", ""));
        prefTimerEnabled = sharedPref.getBoolean("timer_enabled_prefs", false);

        // Only initialize timer if enabled in settings
        if (prefTimerEnabled) {
            prefStartTimerSec = Integer.parseInt(sharedPref.getString("timer_sec_prefs", ""));
            timerSeconds = prefStartTimerSec; // initialize timer variable (first initialization)
        } else {
            // Hide Timer View if not enabled (leave texboxes to keep layout)
            mTimerTitle.setText("");
            mTimerOut.setText("");
        }

        // Fit array to board size
        boardGrid = new int[prefGridSize][prefGridSize];
    }

    private int autoScaledGridCellSize() {
        /** auto scale Grid logic */
        // Required to be called within onStart so can load prior layouts
        // Get dp Height and Width of screen size in case want to modify view
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics); //gets display metrics
        float density = displayMetrics.density; // ex. 1.5
        float pxHeight = displayMetrics.heightPixels;
        float pxWidth = displayMetrics.widthPixels;

        // Log outputs for reference
        Log.i(TAG, "density=" + density);
        Log.i(TAG, "pxHeight=" + pxHeight);
        Log.i(TAG, "pxWidth=" + pxWidth);

        // Declare variables
        int pxCellSize;

        // Logic to determine if Vertical or Horizontal orientation and to adjust accordingly
        if (pxHeight >= pxWidth) {
            Log.i(TAG, "Vertical Orientation");

            // Scale cells based off screen width minus margins in dp
            // Grid boardGrid lines are 6dp, Left and Right margins are each 6dp
            pxCellSize = (int) ((pxWidth - 6*density) / prefGridSize - 6*density);

            Log.i(TAG, "pxCell=" + pxCellSize);
        } else {
            Log.i(TAG, "Horizontal Orientation");

            // Scale cells based off screen height minus menu height and margins in dp
            pxCellSize = (int) ((pxHeight - (6 + 74)*density) / prefGridSize - 6*density); // 74dp titleBar + 6dp grid margin Top & Bottom + 6dp space per line

            Log.i(TAG, "pxCell=" + pxCellSize);
        }
        return pxCellSize;
    }

    public void buildDynamicGrid(GridLayout gridLayout, int cellSize, int savedGridSize) {

        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics); //gets display metrics
        float density = displayMetrics.density; // ex. 1.5

        // Variables
        int width = cellSize;
        int height = cellSize;
        int gridSize = savedGridSize;
        int margin = (int) (6*density); //sets margin in dp, acts as boardGrid lines
        int marginRight, marginBottom; //declare margins
        marginRight = marginBottom = margin; //initialize margins

        // Reset grid layout
        gridLayout.removeAllViews();

        // Set new grid layout
        gridLayout.setRowCount(gridSize); //set grids rows
        gridLayout.setColumnCount(gridSize); //set grids cols
        gridLayout.setBackgroundColor(Color.BLACK); //set background color

        // Populate grid with Buttons
        // Scales to any size grid
        for (int row = 0; row < boardGrid.length; row++) { //loop rows
            for (int col = 0; col < boardGrid.length; col++) { //loop columns
                // generate id name
                String buttonID = String.valueOf(row) + String.valueOf(col);
                final int resID = Integer.valueOf(buttonID);

                // create new button
                final GameButton thisGameButton = new GameButton(this);
                thisGameButton.setBackgroundColor(Color.WHITE);
                thisGameButton.setId(resID);
                thisGameButton.setScaleType(ImageView.ScaleType.CENTER_INSIDE);

                // Save button variables
                thisGameButton.setButtonID(buttonID);
                thisGameButton.setRow(row); //set this buttons row
                thisGameButton.setColumn(col); //set this buttons col

                Log.i(TAG, "create " + buttonID + ", buttonRow=" + thisGameButton.getRow() + ", buttonCol=" + thisGameButton.getColumn());

                // Create event listener for button (duplicates for all buttons)
                thisGameButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Trigger this action if button is clicked
                        nextTurn(thisGameButton);
                    }
                });

                // create new button parameters
                ViewGroup.LayoutParams dimens = new ViewGroup.LayoutParams(width, height);
                GridLayout.LayoutParams gridParam = new GridLayout.LayoutParams(dimens);

                // set margin based on buttons location
                // all left and top margins set to 0
                if (row == 0) { // Target top row
                    if (col == (gridSize - 1)) { //target top corner
                        marginRight = 0;
                        gridParam.setMargins(0, 0, marginRight, marginBottom);
                    } else {
                        gridParam.setMargins(0, 0, marginRight, marginBottom);
                    }
                } else if (row == (gridSize - 1)) { // Target bottom row
                    if (col == (gridSize - 1)) { //target bottom corner
                        marginRight = marginBottom = 0;
                        gridParam.setMargins(0, 0, marginRight, marginBottom);
                    } else {
                        marginBottom = 0;
                        gridParam.setMargins(0, 0, marginRight, marginBottom);
                    }
                } else { // Target all other rows
                    if (col == (gridSize - 1)) { //target right edge
                        marginRight = 0;
                        gridParam.setMargins(0, 0, marginRight, marginBottom);
                    } else {
                        gridParam.setMargins(0, 0, marginRight, marginBottom);
                    }
                }

                gridLayout.addView(thisGameButton, gridParam);
            }
        }
    }


    private void toolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar); // Create toolbar object linked with view

        // Sets Toolbar to act as ActionBar for Activity window
        // Make sure toolbar exists in activity and isn't null
        setSupportActionBar(toolbar);

        // Add back arrow to toolbar
        ActionBar ab = getSupportActionBar(); // Get support ActionBar corresponding to this toolbar
        ab.setDisplayHomeAsUpEnabled(true); // Enable the Up navigation button
    }

    // Inflates menu items
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_page, menu);
        return true;
    }

    /*
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        // Triggers when menu item button is clicked
        // Targets res/menu/menu_main_page.xml
        if (id == R.id.action_new_game) {
            //Display Toast
            Toast.makeText(getApplicationContext(), "New Game!!!", Toast.LENGTH_LONG).show();
            Log.i(TAG, "New Game selected");

            // Go to home
            Intent intent = new Intent(this, IntroActivity.class); // Set intent to move from this activity to other activity
            startActivity(intent); // Trigger intent
        }
        return super.onOptionsItemSelected(item);
    }
    */

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // Save app state before stopping instance
        Log.i(TAG, "onSaveInstanceState");

        //holding values for save
        int[][] saveBoard = boardGrid;
        char saveTurn = PlayerTurn;
        int saveTurnsLeft = turnsLeft;
        int saveScoreX = scoreX;
        int saveScoreO = scoreO;
        int saveThisGameRound = thisGameRound;
        String saveTitleMessage = titleMessage;
        String saveSubTitleMessage = subTitleMessage;
        boolean saveAnimationActivated = animationActivated;

        // Kill timer and save variable
        mHandler.removeCallbacks(timerThread);
        Log.i(TAG, "Timer Stopped");
        int saveTimerSeconds = timerSeconds;

        //save variables
        outState.putSerializable("saveBoard", saveBoard);
        outState.putChar("saveTurn", saveTurn);
        outState.putInt("saveTurnsLeft", saveTurnsLeft);
        outState.putInt("saveScoreX", saveScoreX);
        outState.putInt("saveScoreO", saveScoreO);
        outState.putInt("saveThisGameRound", saveThisGameRound);
        outState.putInt("saveTimerSeconds", saveTimerSeconds);
        outState.putString("saveTitleMessage", saveTitleMessage);
        outState.putString("saveSubTitleMessage", saveSubTitleMessage);
        outState.putBoolean("saveAnimationActivated", saveAnimationActivated);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.i(TAG, "onRestoreInstanceState");

        // Recover saved variables
        boardGrid = (int[][]) savedInstanceState.getSerializable("saveBoard");
        Log.i(TAG, "boardGridArray" + boardGrid);
        PlayerTurn = savedInstanceState.getChar("saveTurn");
        turnsLeft = savedInstanceState.getInt("saveTurnsLeft");
        scoreX = savedInstanceState.getInt("saveScoreX");
        scoreO = savedInstanceState.getInt("saveScoreO");
        thisGameRound = savedInstanceState.getInt("saveThisGameRound");
        timerSeconds = savedInstanceState.getInt("saveTimerSeconds");
        animationActivated = savedInstanceState.getBoolean("saveAnimationActivated");
        titleMessage = savedInstanceState.getString("saveTitleMessage");
        subTitleMessage = savedInstanceState.getString("saveSubTitleMessage");

        // Restore visuals
        if (PlayerTurn == 'x') {
            mPlayerTurnOut.setText(R.string.x_player_turn); // set player text
        } else {
            mPlayerTurnOut.setText(R.string.o_player_turn); // set player text
        }

        gameScore = scoreX + ":" + scoreO;
        mScoreOut.setText(gameScore); // restore gameScore text

        restore(boardGrid); // restore button images using array as template

        // activate round winner animation
        if (animationActivated) {
            mTxtZoomInTitle.setText(titleMessage);
            mTxtZoomInSubTitle.setText(subTitleMessage);
            if (titleMessage == ""){
                mTxtZoomInTitle.setVisibility(View.GONE); //hides title textbox if empty
            }
            roundWinnerAnimation();
            animationActivated = false;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "On Resume");

        // Restart timer if enabled
        // Required to start after instance Variables restored
        if (prefTimerEnabled && !timerPaused) {
            mHandler.post(timerThread); // start timer thread
            Log.i(TAG, "Timer Started");
        }
    }

    // Called when user turns off / leaves app while still running
    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG, "onGamePause");
        //Kill timer
        mHandler.removeCallbacks(timerThread);
        timerPaused = true; // prevents onResume from re-starting timer
        Log.i(TAG, "Timer Paused");
    }

    // Called when user turns on / resumes app while still running
    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i(TAG, "onGameRestart");

        // Call custom pause dialog popup
        PauseCustomDialog pauseDialog = new PauseCustomDialog(this, mHandler, timerThread);

        // activates if user doesn't hit a button
        // this must be set outside of the dialog class
        pauseDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                Log.i(TAG, "Game Resumed");
                mHandler.post(timerThread);
            }
        });
        pauseDialog.show();
    }

    // Called when user restores view
    public void restore(int[][] array) {
        int imgTagVal;
        for (int row = 0; row < array.length; row++) {
            for (int col = 0; col < array.length; col++) {
                String buttonID = String.valueOf(row) + String.valueOf(col);
                final int resID = Integer.valueOf(buttonID);
                thisButton = (GameButton) findViewById(resID); //target object
                imgTagVal = array[row][col]; //get array value

                Log.i(TAG, "buttonID=" + buttonID + ", buttonRow=" + thisButton.getRow() + ", buttonCol=" + thisButton.getColumn()
                        + ", RestoreImgTagValue=" + imgTagVal);// Log values

                // Switch based on array value
                // Legend: 0 = exitGame image, 1 = first players image, -1 = second players image
                switch (imgTagVal) {
                    case 0:
                        // No image
                        break;

                    case 1:
                        thisButton.setImageResource(R.drawable.x_graphic); // placeholder for first image
                        Log.i(TAG, "buttonID=" + buttonID + ", Set as X");// Log values
                        thisButton.setEnabled(false); // disable button
                        break;

                    case -1:
                        thisButton.setImageResource(R.drawable.o_graphic); // placeholder for second image
                        Log.i(TAG, "buttonID=" + buttonID + ", Set as O");// Log values
                        thisButton.setEnabled(false); // disable button
                        break;

                    default:
                        // No change
                }
            }
        }
    }

    // Next Turn logic with exitGame button press (Overload constructor)
    // Triggered when timer runs out before any selection
    // Note: if not timer exclusive then need to check if timer enabled before restart timer
    public void nextTurn() {

        // save which player timed out
        String snackbarText = String.valueOf(PlayerTurn).toUpperCase() + " " + getText(R.string.player_timed_out);

        // skip player's turn
        if (PlayerTurn == 'x') {
            PlayerTurn = 'o'; // increment to next player
            mPlayerTurnOut.setText(R.string.o_player_turn); // update player text
        } else {
            PlayerTurn = 'x'; // increment to next player
            mPlayerTurnOut.setText(R.string.x_player_turn); // update player text
        }

        // Create Snackbar popup
        Snackbar snackbar;
        snackbar = Snackbar.make(findViewById(R.id.myMainActivityLayout), snackbarText,
                Snackbar.LENGTH_SHORT);

        // Center Snackbar text
        View view = snackbar.getView();
        TextView txtV = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        txtV.setGravity(Gravity.CENTER_HORIZONTAL);
        txtV.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

        // Show Snackbar popup
        // Reports which player timed out
        snackbar.show();

        // restart timer
        restartTimer();

    }

    // Next Turn logic
    public void nextTurn(GameButton thisButton) {
        // increment turns down
        turnsLeft--;

        // determine players turn
        if (PlayerTurn == 'x') {
            thisButton.setImageResource(R.drawable.x_graphic); // set image
            boardGrid[thisButton.getRow()][thisButton.getColumn()] = 1; // store image ref value to array
            PlayerTurn = 'o'; // increment to next player
            mPlayerTurnOut.setText(R.string.o_player_turn); // update player text
        } else {
            thisButton.setImageResource(R.drawable.o_graphic);
            boardGrid[thisButton.getRow()][thisButton.getColumn()] = -1; // store image ref value to array
            PlayerTurn = 'x'; // increment to next player
            mPlayerTurnOut.setText(R.string.x_player_turn); // update player text
        }

        thisButton.setEnabled(false); // disable button so can't re-select

        Log.i(TAG, "buttonID=" + thisButton.getButtonID() + ", row=" +thisButton.getRow() + ", col=" +thisButton.getColumn() + ", set ImgTagValue=" + boardGrid[thisButton.getRow()][thisButton.getColumn()]);// Log values

        // check if have winner
        checkScore(turnsLeft, thisButton);

        // only restart timer if enabled in preferences
        if (prefTimerEnabled){
            restartTimer();
        }
    }

    // Scoring logic
    public void checkScore(int turnsLeft, GameButton thisButton) {
        int buttonRow = thisButton.getRow();
        int buttonCol = thisButton.getColumn();
        int diagSum = 0, antiDiagSum = 0, rowSum = 0, colSum = 0;
        int winner = 0; // default value if no winner
        int size = prefGridSize;

        // each sum is the sum of that row/col/diagonal values
        // the individual values can be 1, 0 , -1 which indicates X image, empty, O image
        // by summing up these values we can see if they are in a row
        // Example: three O's in a row = -1-1-1 = |-3| = 3 = board size (3)
        // outputs are -1, 0, 1 where -1 indicates O won, 1 indicates X won, and 0 indicates nobody won this turn

        for (int i = 0; i < boardGrid.length; i++) {
            //Check Row
            rowSum += boardGrid[buttonRow][i];
            if (Math.abs(rowSum) == size) {
                winner = rowSum / size;
                Log.i(TAG, "Winner is " + winner);
                break; // have winner don't need to continue logic
            }

            //Check Col
            colSum += boardGrid[i][buttonCol];
            if (Math.abs(colSum) == size) {
                winner = colSum / size;
                Log.i(TAG, "Winner is " + winner);
                break; // have winner don't need to continue logic
            }

            //Check Diagonal
            if (buttonRow == buttonCol) {
                diagSum += boardGrid[i][i];
                if (Math.abs(diagSum) == size) {
                    winner = diagSum / size;
                    Log.i(TAG, "Winner is " + winner);
                    break; // have winner don't need to continue logic
                }
            }

            //Check anti-diagonal
            if (buttonRow+buttonCol == size-1) {
                antiDiagSum += boardGrid[i][(size-1) - i];
                if (Math.abs(antiDiagSum) == size) {
                    winner = antiDiagSum / size;
                    Log.i(TAG, "Winner is " + winner);
                    break; // have winner don't need to continue logic
                }
            }
        }

        if (winner != 0) {
            // pass winner output to winnerOutput method
            winnerOutput(winner);
        } else if (turnsLeft == 0) {
            // You got a draw
            Log.i(TAG, "You Draw");
            winnerOutput(0);
        }

        // else nobody won and wait for next turn
    }

    public void winnerOutput(int winner) {
        titleMessage = "Congrats!!!";
        subTitleMessage = "";

        // winner can be 1 = X won, -1 = O won, or 0 = Draw
        switch (winner) {
            case 1:
                subTitleMessage = "X Player Won";
                scoreX++;
                thisGameRound++; // increment round
                break;
            case -1:
                subTitleMessage = "O Player Won";
                scoreO++;
                thisGameRound++; // increment round
                break;
            case 0:
                titleMessage = ""; // hides title
                subTitleMessage = "You Tied!!!";
                // when tie don't change score or increment round
                break;
        }

        // Check if won round or won game
        // See if won majority of rounds within max number of rounds
        if (Math.abs(scoreO - scoreX) > prefGameRounds/2 || scoreO + scoreX == prefGameRounds) { // triggered if won game

            if (scoreX > scoreO) {
                subTitleMessage = "X Player Won";
            } else if (scoreX < scoreO) {
                subTitleMessage = "O Player Won";
            } else {
                titleMessage = ""; // hides title
                subTitleMessage = "You Tied!!!";
            }

            // Pass message values to Winner page
            Intent intent = new Intent(this, WinnerActivity.class); // Set intent to move from this activity to other activity
            intent.putExtra("titleMessageKey", titleMessage);
            intent.putExtra("subTitleMessageKey", subTitleMessage);
            startActivity(intent); // Trigger intent
            this.finish(); // kill this page

        } else { // triggered if won round

            Log.i(TAG, "round won");

            // call round winner animation - gets saved and run on restore
            mTxtZoomInTitle.setText(titleMessage);
            mTxtZoomInSubTitle.setText(subTitleMessage);
            animationActivated = true;

            gameScore = scoreX + ":" + scoreO; // update game score
            mScoreOut.setText(gameScore); // set game score
            resetBoard(); // reset board
        }
    }

    public void resetBoard (){
        boardGrid = new int[prefGridSize][prefGridSize]; // reset stored board values
        turnsLeft = prefGridSize * prefGridSize; // reset turns left
        recreate(); // closes and reopens view

    }

    // Recursive thread to create Timer
    // This results in a loop where the thread will repeat until the timer completes or is stopped
    private Runnable timerThread = new Runnable() {
        @Override
        public void run() {

            if (timerSeconds > 0) {
                // Runs each tick
                String textString = String.valueOf(timerSeconds); // ensure int converted to String
                mTimerOut.setText(textString); // update output with current seconds value
                if (timerSeconds <= 5) {
                    // Highlight text when running out of time
                    mTimerOut.setTextColor(Color.RED);
                    mTimerOut.setTypeface(null, Typeface.BOLD);
                } else {
                    // Reset to defaults
                    mTimerOut.setTextColor(Color.BLACK);
                    mTimerOut.setTypeface(null, Typeface.NORMAL);
                }
                mHandler.postDelayed(this, 1000); // re-run thread with 1 sec delay (The tick)

                //countdown seconds since thread loops
                timerSeconds--;
            } else {
                // Runs on completion
                nextTurn(); // skip persons turn
            }

        }

    };

    private void restartTimer() {
        // Kill any timer thread
        mHandler.removeCallbacks(timerThread);
        Log.i(TAG, "Timer Stopped");

        // Reset timer variables
        timerSeconds = prefStartTimerSec;

        // ReStart timer Thread through handler
        mHandler.post(timerThread);
        Log.i(TAG, "Timer Started");
    }

    public void roundWinnerAnimation() {
        mTxtZoomIn.startAnimation(animZoomIn); //start animation
        mTxtZoomIn.setVisibility(View.VISIBLE); //changes to 0 alpha after animation
    }

}
