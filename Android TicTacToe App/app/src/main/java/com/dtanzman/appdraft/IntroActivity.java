package com.dtanzman.appdraft;

import android.content.Intent;
import android.preference.PreferenceActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class IntroActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
    }

    /** Called when user taps "Click To Play" button */
    public void goToGame(View view) {
        Intent intent = new Intent(this, myMainActivity.class); // Set intent to move from this activity to other activity
        startActivity(intent); // Trigger intent
    }

    // Called when hit Settings button
    public void goToSettings(View view) {
        Intent intent = new Intent(this, PrefActivity.class); // Set intent to move from this activity to other activity
        startActivity(intent); // Trigger intent
    }
}
