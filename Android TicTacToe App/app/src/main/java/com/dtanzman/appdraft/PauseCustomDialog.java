package com.dtanzman.appdraft;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by dtanzman100 on 4/9/17.
 */

public class PauseCustomDialog extends Dialog implements android.view.View.OnClickListener {
    private static final String TAG = "myMainActivity"; // Enables filtering log messages in log output
    private Activity activity;
    private Button resumeGame, exitGame;
    private Handler mHandler;
    private Runnable timerThread;

    public PauseCustomDialog(Activity activity, Handler mHandler, Runnable timerThread) {
        super(activity);
        this.activity = activity;
        this.mHandler = mHandler;
        this.timerThread = timerThread;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.popup);
        resumeGame = (Button) findViewById(R.id.right_popup_button);
        exitGame = (Button) findViewById(R.id.left_popup_button);
        resumeGame.setOnClickListener(this);
        exitGame.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.right_popup_button:
                dismiss(); // dismisses dialog box
                Log.i(TAG,"Game Resumed");
                mHandler.post(timerThread);
                break;
            case R.id.left_popup_button:
                Log.i(TAG,"Game Closed");
                activity.finish(); // kills activity
                break;
            default:
                break;
        }
        dismiss();
    }
}
