const express = require('express');
const bodyParser = require('body-parser');
const exphbs = require('express-handlebars');
const passport = require('passport');
const flash = require('connect-flash');
const expMsgs = require('express-messages');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const morgan = require('morgan'); // Optional to log POST & GET requests
const momemt = require('moment'); // to format dates
const expressSanitizer = require('express-sanitizer'); //sanitizes data to prevent injection atacks

const app = express();

/*** Configuration ***/

// define static resource location
app.use(express.static(__dirname + '/resources'));

// setup application
app.use(morgan('dev')); // log every request to the console (Middleware logging system) (Optional)
app.use(cookieParser()); // read cookies (required for auth)

// setup body-parser
app.use(bodyParser.json()); // parse application/json
app.use(bodyParser.urlencoded({ extended: false })); // parse application/x-www-form-urlencoded

// mount express-sanitizer (after body-parser)
app.use(expressSanitizer());

// setup handlebars view engine
let hbs = exphbs.create({ 
	// custom handlebars helper
	helpers: { 
		// format date
		shortDate: (date) => { 
			return momemt.utc(date).format('M/D/YY');
		},
		datePickerFormat: (date) => {
			if (date != null) {
				return momemt.utc(date).format('YYYY-MM-DD');
			}
		}
	},
	// default layout template
	defaultLayout: 'index' 
});
app.engine('handlebars', hbs.engine); // add custom helper
app.set('view engine', 'handlebars');

// setup passport
require('./config/passport')(passport); //pass passport configuration
app.use(session({ 
		secret: 'mySuperSecretCookieSecret', //sign the session ID
		resave: false, //if true can cause race conditions where changes try to save at same time
		saveUninitialized: false, //false reduces server storage and usage for logins
		cookie: {maxAge: 1000*60*60*12} //(12h) cookie life in milliseconds - set to minimize hijacking login session
	}));
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session
app.use((req, res, next) => { // enables connect-flash with handlebars
  res.locals.messages = expMsgs(req, res);
  next();
});

/*** Routes ***/
require('./routes/index')(app, passport);

// Shows 404 error if can't find link
app.use((req, res) => {
    res.status(404);
    res.render('404');
});

/*** Launch ***/
const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log('http://localhost:'+ port);
});

