const mongoose = require('mongoose');
const credentials = require("./dbCredentials.js");
const bcrypt = require('bcrypt');

// setup credentials (leveraged format from Mod3 samples)
const dbUrl = 'mongodb://' + credentials.username +
	':' + credentials.password + '@' + credentials.host + ':' + credentials.port + '/' + credentials.database;

let connection = null;
let model = null;

const Schema = mongoose.Schema;

mongoose.Promise = global.Promise;

// defines db schemas
const taskSchema = new Schema({ //child schema to hold user's tasks (aka: Sub Docs)
    task: String,
    dueDate: {type: Date, default: null},
    createDate: {type: Date, default: Date.now},
    completed: {type: Boolean, default: false}
})

const userSchema = new Schema({ //parent schema to hold user account
    email: {type: String, unique: true, required: true}, // email is unique & required
    password: {type: String, required: true}, // pass is required
    admin: {type: Boolean, default: false}, // can manually update in db
    tasks: [taskSchema]
}, { versionKey: '_version'}); //overides default version key from (__V) - prevents logic conflicts


/*** export connection ***/
module.exports.getModel = 
	() => {
		if (connection == null) {
			console.log("Creating connection and model...");
			connection = mongoose.createConnection(dbUrl,
                // handle errors
                (err)=>{
                    if(err)
                        console.log("DB Error:\n",err);
                    }
                );
			model = connection.model("Users_Tanzman", userSchema);
		};
		return model;
	};

















