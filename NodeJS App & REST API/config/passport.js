const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcrypt');

// load user model
const DB = require('../config/dbConnection.js');
const Users = DB.getModel(); //target db objects

module.exports = (passport) => {

  /*** passport session setup ***/
  // required for persistent login sessions
  // passport needs ability to serialize and unserialize users in/out of the session

  // used to serialize the user for the session
  passport.serializeUser((user, done) => {
    done(null, user.id);
  });

  // used to deserialize the user
  passport.deserializeUser((id, done) => {
    Users.findById(id, (err, user) => {
      done(err, user);
    });
  });


  /*** local-login strategy ***/
  passport.use('local-login', new LocalStrategy({
      // by default, uses username and password, override parameter to email
      usernameField : 'email',
      passwordField : 'password',
      passReqToCallback : true // passes back entire request to the callback (req for overide)
  },
  (req, email, password, done) => { // gets email and password from login form
      // sanitize variables
      let sEmail = req.sanitize(email);
      let sPass = req.sanitize(password);

      // validate login
      Users.findOne({ 'email' :  sEmail }, (err, user) => {
        // pass errors to passport
        if (err){
          return done(err);
        }
        // validate user exists
        if (!user){
          return done(null, false, req.flash('loginError', 'No user found.'));
        }
        // validate password (async)
        bcrypt.compare(sPass, user.password, (err, res) => {
          if(err){
            return done(err);
          }
          if(res){
            // validated successfully, return user object
            return done(null, user);
          }else{
            return done(null, false, req.flash('loginError', 'Oops! Wrong password.'));
          }
        });
      });

  }));

};