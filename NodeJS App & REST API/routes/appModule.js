const DB = require('../config/dbConnection.js');
const Users = DB.getModel(); //target db objects
const passport = require('passport'); //required for passport authentication
const js2xmlparser = require("js2xmlparser"); //simplifies converting to XML
const validator = require("email-validator"); //validates emails
const bcrypt = require('bcrypt'); //encrypts passwords

/*** Authentication ***/
// route middleware to make sure a user is logged in
module.exports.isLoggedIn = 
	(req, res, next) => {
	    // if user is authenticated in the session, carry on 
	    if (req.isAuthenticated()){
	    	// trigger next function
	    	return next();
	    }
	    // if not authenticated redirect
	    res.redirect('/login');
	};

module.exports.isAdmin = 
	(req, res, next) => {
		if (req.user.admin)
			return next();

		// not logged in as admin
		res.redirect('/tasks');
	};

module.exports.disableAPI 
	= (req, res, next) => {
		//create json object
		let jsonObj = {admin: 'Admin disabled in API'};

	  //respond based on format
		res.format({
			'text/html': () => {
				// trigger next function
				return next();
			},
			'application/json': () => {
				//pass json output
				res.json(jsonObj);
			},
			'application/xml': () => {
				// create child elements JSON array (parse/stringify removes invalid xml chars)
				let childElems = JSON.parse(JSON.stringify(jsonObj));

				// parse to XML with root element as "user"
				let xml = js2xmlparser.parse("user",childElems);

				// pass XML output
				res.send(xml);
			},
			'default': () => {
				res.status(404);
				res.send("<b>404 - Not Found</b>");
			}	
		});
	};

module.exports.login = 
	(req, res) => {
			// render the page and pass in any flash messages 
			let errMsg = req.flash('loginError');
			let sucMsg = req.flash('successMessage');

			//create json object
			let jsonObj = {error: errMsg, success: sucMsg, login: 'Please login'};

			//respond based on format
			res.format({
				'text/html': () => {
					// go to view template
			  	res.render('login', 
			  		{title: "User Login", error: errMsg, success: sucMsg, 
			  		showNewUserLink: true, hideLogoutLink: true}); 
				},
				'application/json': () => {
					//pass json output
					res.json(jsonObj);
				},
				'application/xml': () => {
					// create child elements JSON array (parse/stringify removes invalid xml chars)
					let childElems = JSON.parse(JSON.stringify(jsonObj));

					// parse to XML with root element as messages
					let xml = js2xmlparser.parse("messages",childElems);

					// pass XML output
					res.send(xml);
				},
				'default': () => {
					res.status(404);
					res.send("<b>404 - Not Found</b>");
				}	
			});
    	
  };

module.exports.logout = 
	(req, res) => {
			// destroy cookie session - logging out
			console.log("Congrats You Logged Out!")
      req.session.destroy((err) => {
          res.redirect('/login');
      });
  };

/*** New Users ***/
module.exports.newUser = 
	(req , res , next) => {
			// store error messages in a String
			let errMsg = req.flash('signupError');

			//create json object
			let jsonObj = {error: errMsg};

	  	//respond based on format
			res.format({
				'text/html': () => {
					// go to newUserView template
			  	res.render('newUserView', 
			  		{title:"New User", message: errMsg, hideLogoutLink: true});
				},
				'application/json': () => {
					//pass json output
					res.json(jsonObj);
				},
				'application/xml': () => {
					// create child elements JSON array (parse/stringify removes invalid xml chars)
					let childElems = JSON.parse(JSON.stringify(jsonObj));

					// parse to XML with root element as "messages"
					let xml = js2xmlparser.parse("messages",childElems);

					// pass XML output
					res.send(xml);
				},
				'default': () => {
					res.status(404);
					res.send("<b>404 - Not Found</b>");
				}	
			});
	};

module.exports.saveNewUser = 
	(req , res , next) => {
			// sanitize inputs
			let email = req.sanitize(req.body.email);
			let password = req.sanitize(req.body.password);

			// validate inputs
			if(email == null || password == null){ 
				// if didn't inputs both values
				console.log('Error: Input both email and password values');
        req.flash('signupError', 'Input both email and password values.');
        // exit back to newUser page
        return res.redirect('/newUser');
			} else if (!(validator.validate(email))) { 
				// validate email 
      	console.log('Error: Email Invalid');
        req.flash('signupError', 'Input a valid email.');
        // exit back to newUser page
        return res.redirect('/newUser');
      }

      // verify new email is unique
	    Users.findOne({'email': email}, (err, user) => {
          if (err){
          	// handle db error
          	req.flash('signupError','%s',err);
            // exit back to newUser page
        		return res.redirect('/newUser');
          }
          	
          // check if email already exists
          if (user) {
          	console.log('Error: Email Already Exists');
            req.flash('signupError', 'That email is already taken.');
            // exit back to newUser page
            return res.redirect('/newUser');
          } else {
          	// set salt
          	const saltRounds = 8;

          	// generate hashed password (async)
          	bcrypt.hash(password, saltRounds, (err, hash) => {
          		if(err){
          			console.log('%s',err);
            		req.flash('signupError', '%s',err);
          		}
          		// if no user with that email create new user (inputs already sanitized)
	            let newUser = new Users();
	            newUser.email = email;
	            newUser.password = hash;

	            console.log('Plain-Password:', password);
	            console.log('Hashed-Password:', hash);

	            // save the user
	            newUser.save((err) => {
			          if (err){
			          	req.flash('loginError','%s',err);
			          	req.flash('adminError','%s',err);
			            console.log("Error updating : %s ",err );
			          }
			          console.log('Saved New User Triggered:',newUser);
			          // feedback to users
			          req.flash('successMessage', 'New User succesfully created');
			          // go back to admin or login (if admin)          
			          res.redirect('/admin');
			        });

          	});
          }
        });

	};

/*** Admin ***/
module.exports.displayAdmin = 
	(req , res , next) => {	
		// render the page and pass in any flash messages 
		let errMsg = req.flash('adminError');
		let sucMsg = req.flash('successMessage');

		// Return all users (whole DB)
		Users.find({}, (err, users) => {
      if (err) {
      	// handle db error
      	req.flash('loginError','%s',err);
        // exit back to newUser page
    		return res.redirect('/login');
      }
      //render admin view 
      res.render('adminView',
    		{title:"Admin", error: errMsg, success: sucMsg, users: users, 
    		userCount: users.length, showTaskLink: true, showNewUserLink:true});
    });
	}

module.exports.editUser = 
	(req , res , next) => {
			// get user _id
			let id = req.sanitize(req.params.id);

			// render the page and pass in any flash messages 
			let errMsg = req.flash('adminError');

			// find user from ID
	    Users.findById(id, (err, user) => {
	    	if (err){
	    		// handle db error
        	req.flash('adminError','%s',err);
          // exit back to page
      		return res.redirect('/admin');
	    	}

	      // pass user variables to page
     		res.render('editUserView',
        {title:"Edit User", user: user, error: errMsg});

	    });  			
	};

module.exports.saveEditUser = 
	(req , res , next) => {
			// sanitize inputs
	    let id = req.sanitize(req.params.id);
	    let email = req.sanitize(req.body.email);
			let password = req.sanitize(req.body.password);
			const saltRounds = 8; // set salt for hashing password

	    // validate inputs
			if(email == null || password == null){ 
				// if didn't inputs both values
				console.log('Error: Input both email and password values');
        req.flash('adminError', 'Input both email and password values.');
        // exit back to newUser page
        return res.redirect('/admin/edit/'+id);
			} else if (!(validator.validate(email))) { 
				// validate email 
      	console.log('Error: Email Invalid');
        req.flash('signupError', 'Input a valid email.');
        // exit back to newUser page
        return res.redirect('/admin/edit/'+id);
      }

	    // find user from ID
	    Users.findById(id, (err, user) => {
	    	// verify can find id
	    	if (err){
        	req.flash('adminError','%s',err);
          // exit back to page
      		return res.redirect('/admin');
	    	}

      	// generate hashed password (async)
      	bcrypt.hash(password, saltRounds, (err, hash) => {
      		if(err){
      			console.log('%s',err);
        		req.flash('signupError', '%s',err);
        		return res.redirect('/admin');
      		}

      		// revise variables
		      user.email = email;
		      user.password = hash;

          console.log('Plain-Password:', password);
          console.log('Hashed-Password:', hash);

          // save all changes to user object
		      user.save((err) => {
		        if (err){
		        	req.flash('adminError','%s',err);
		          console.log("%s", err);
		        }
		        //async so only load after saved to db
		        res.redirect('/admin');
		      });

      	});

	    }); 
   
	};

module.exports.deleteUser = 
	(req , res , next) => {
	    
	    let id = req.sanitize(req.params.id);

	    Users.findById(id,  (err, user) => {
	      if(err)
	        console.log("Error Selecting : %s ", err); 

	      if (!user)
	        return res.render('404');
	     
	      // delete user
	      user.remove( (err) => {
	        if (err){
	        	req.flash('adminError','%s',err);
	          console.log("Error deleting : %s ",err );
	        }
	      	res.redirect('/admin');
	      });        
	    });
  };	

/*** Tasks ***/
module.exports.displayTasks = 
	(req , res , next) => {
			// render the page and pass in any flash messages 
			let errMsg = req.flash('tasksError');
			let sucMsg = req.flash('successMessage');

			// get user data
			let user = req.user;

			//sort tasks by due date (ascending)
	    let tasks = user.tasks;
	    tasks.sort((a,b)=>{
	    	let dateA = new Date(a.dueDate), dateB = new Date (b.dueDate);
	    	return dateA - dateB; //ascending
	    });

	    //create json object
			let jsonObj = {tasks: tasks, messages: {sucess: sucMsg, error: errMsg}};

	    //respond based on format
			res.format({
				'text/html': () => {
					res.render('displayTasksView',
	      		{title:"Task List", tasks:tasks, showAdminLink:user.admin});
				},
				'application/json': () => {
					//pass json output
					res.json(jsonObj);
				},
				'application/xml': () => {
					// create child elements JSON array (parse/stringify removes invalid xml chars)
					let childElems = JSON.parse(JSON.stringify(jsonObj));

					// parse to XML with root element as "user"
					let xml = js2xmlparser.parse("user",childElems);

					// pass XML output
					res.send(xml);
				},
				'default': () => {
					res.status(404);
					res.send("<b>404 - Not Found</b>");
				}	
			});
	    
	};

module.exports.addTask = 
	(req , res , next) => {
			// go to addTaskView template
	  	res.render('addTaskView', 
	  		{title:"Add Task"});
	};

module.exports.saveTask = 
	(req , res , next) => {

	    let user = req.user;

	    // add new child (to Top)
      user.tasks.unshift({
      	task: req.sanitize(req.body.task),
      	dueDate: req.sanitize(req.body.due)
      });

      // get modified task and set as new
      let subdoc = user.tasks[0];
      subdoc.isNew; //true

      // save all changes by calling save on parent object
      user.save((err) => {
        if (err){
        	req.flash('tasksError','%s',err);
          console.log("Error updating : %s ",err );
        }
        res.redirect('/tasks');
      });

	  };

module.exports.editTask = 
	(req , res , next) => {
			// sanitize task _id
	    let id = req.sanitize(req.params.id);

	    let user = req.user;

	    // find task from task id
	    let task = user.tasks.id(id);

     	// pass child variables to page
     	res.render('editTaskView',
        {title:"Edit Task", task: {id: task._id, task: task.task, due: task.dueDate}});
	};

module.exports.saveEditTask = 
	(req , res , next) => {
		// sanitize task _id
    let id = req.sanitize(req.params.id);

    let user = req.user;

    // find task from task id
    let task = user.tasks.id(id);

    // verify task id exits
    if(task != null){
    	// update parameters
      task.task = req.sanitize(req.body.task);
      task.dueDate = req.sanitize(req.body.due);

      // save all changes by calling save on parent object
      user.save((err) => {
        if (err) {
          console.log("Error updating : %s ",err );
        	req.flash('tasksError', 'Error saving task');
        }
        res.redirect('/tasks');
      });
    } else {
    	console.log('Error: task _id doesn\'t exist');
    	req.flash('tasksError', 'Task _id doesn\'t exist');
    	res.redirect('/tasks');
    }  	
  };

module.exports.deleteTask = 
	(req , res , next) => {	    
    // sanitize user input
    let id = req.sanitize(req.params.id);

    let user = req.user;

    let task = user.tasks.id(id);

    // task exists
    if(task != null){
    	// remove child doc from parent based on child_id
      task.remove((err) => {
      	if (err){
          console.log("Error removing task : %s ",err );
          req.flash('tasksError', 'Error removing task');
      	}
      });

      // save all changes by calling save on parent object
      user.save((err) => {
        if (err){
        	console.log("Error updating : %s ",err );
        	req.flash('tasksError', 'Error saving task');
        }
        res.redirect('/tasks');
      });
    } else {
    	console.log('Error: task _id doesn\'t exist');
    	req.flash('tasksError', 'Task _id doesn\'t exist');
    	res.redirect('/tasks');
    }
    
	};



  