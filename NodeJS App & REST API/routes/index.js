// router routes
module.exports = (app, passport) => {

	// get functions
	const appModule = require("./appModule");

	app.get('/', (req, res, next) => {
	  res.redirect('/tasks');
	});

	app.get('/tasks', appModule.isLoggedIn,	appModule.displayTasks);

	app.get('/tasks/add', appModule.isLoggedIn, appModule.addTask);
	app.post('/tasks/add', appModule.isLoggedIn, appModule.saveTask);

	app.get('/tasks/edit/:id', appModule.isLoggedIn, appModule.editTask);
	app.post('/tasks/edit/:id', appModule.isLoggedIn, appModule.saveEditTask);

	app.get('/tasks/delete/:id', appModule.isLoggedIn, appModule.deleteTask);

	app.get('/newUser', appModule.newUser);
	app.post('/newUser', appModule.saveNewUser);

	app.get('/admin', appModule.isLoggedIn, appModule.disableAPI, appModule.isAdmin, appModule.displayAdmin);

	app.get('/admin/edit/:id', appModule.isLoggedIn, appModule.disableAPI, appModule.isAdmin, appModule.editUser);
	app.post('/admin/edit/:id', appModule.isLoggedIn, appModule.disableAPI, appModule.saveEditUser);

	app.get('/admin/delete/:id', appModule.isLoggedIn, appModule.disableAPI, appModule.isAdmin, appModule.deleteUser);

	app.get('/login', appModule.login);
	// uses passed passport configuration (so can't embed in appModule) (logic in passport.js)
	app.post('/login', passport.authenticate('local-login', {
	      successRedirect : '/tasks', // redirect on success
	      failureRedirect : '/login', // redirect on fail
	      failureFlash : true // allow flash messages
	}));

	app.get('/logout', appModule.logout);
};
