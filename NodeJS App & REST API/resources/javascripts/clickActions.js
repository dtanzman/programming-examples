addTask = () => {
  window.location.href = '/tasks/add';
}

goBack = () => {
	//trigger browser back button
	window.history.back();
}

editTask = (id) => {
	window.location.href = '/tasks/edit/'+id;
}

deleteTask = (id) => {
	window.location.href = '/tasks/delete/'+id;
}

editUser = (id) => {
	window.location.href = '/admin/edit/'+id;
}

deleteUser = (id) => {
	window.location.href = '/admin/delete/'+id;
}
