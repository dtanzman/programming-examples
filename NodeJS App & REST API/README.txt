About:

	For this term project I created a cross-platform task planner that doubles as a REST API for outputs in JSON and XML. An regular user can create an account, login, add tasks, edit tasks, and delete tasks. An admin user can additionally view all users, edit users email/pass, and delete users. To create a new admin account manually set the users admin property as true within the mongodb.

	Note: For security purposes I disabled the REST API's admin features. To access those features login as an admin from a (Chrome) browser.

To Run Server:

	'node server.js'

Default Users:

	email: admin@email.com
	pass:  admin
	admin: true

	email: test@email.com
	pass:  test
	admin: false