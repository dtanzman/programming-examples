/* JavaScript & jQuery for back_button */

/* when document ready load button function to go back 1 page in history when clicked */
$(document).ready(function() {
    $('#back_button').click(function () {
        parent.history.back();
        return false;
    });
});
