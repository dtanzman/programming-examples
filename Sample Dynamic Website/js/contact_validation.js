/* Author     : David Tanzman */
/* Form Validation for contact submission */


// wait for window onload so that DOM is initialized
window.onload = function () {
    // run form validation when initialize submit
    $("#my_form").on('submit', validateForm);
    
    // run validation on each filed change immediately as well
    // check all of our validations
    $.each(validations, function(selector, validator){
        // our keys represent the html id of the field, which in css selector 
        // requires the "#" prefix
        $field = $('#'+selector);

        // if the field changes try to validate it
        $field.on('change', function() {
            // every function called this way has "this" refering to that element
            // so we pass that element wrapped in jquery to the validator
            validateField($(this), validator);
        });
    });
};

/*
 * This gets called when the form is submitted
 * It loops over all the keys from the validations object
 * and it calls validateField using the key as the id for the field
 * and the value as the function that should be used to check the fields value
 */
function validateForm(e) {
    // assume form is valid until bad field is found
    var isValid = true;

    // check all of our validations
    $.each(validations, function(selector, validator){
        // our keys represent the html id of the field, which in css selector 
        // requires the "#" prefix
        // if the field wasnt validated, then we set isValid to false
        // var uses $ infront to indicate it's jQuery and not HTML ($field)
        var $field = $('#'+selector);
        if (!validateField($field, validator)) {
            isValid = false;
        }
    });
    
    if (!isValid) {
        // stop submission
        return false;
    }
}

/* 
 * validates field, accepts jQuery object for field being validated, 
 * validator function which returns boolean true if valid
 * otherwise a string with the error to display
 */
function validateField($field, validator) {
    // use the supplied validator
    var result = validator($field.val());
    
    // validator gives true if good, otherwise its an error
    var valid = (result === true);

    // clear the last round of validation in case
    $field.parent().find('.warning').remove();
    $field.parent().removeClass('valid');

    // if validator came back as true
    if (valid) {
        // add the valid class to the parent and remove warning if was there
        $field.parent().addClass('valid');
    } else {
        // new span to hold the warning
        var warningSpan = $('<span class="warning">');
        
        // populate it with the result of the validator
        warningSpan.text(result);
        
        // append it to the title
        $field.parent().find('.title').append(warningSpan);
    }
    
    // send back the status to the caller
    return valid;
}


/* 
 * List of all fields needing validation, and their respective 
 * validators. Validators return true if good, or a string 
 * representing the error if not.
 */
var validations = {

    // first_name has a minimum of 2 char
    first_name: function(first_name) {
        if (first_name.length < 2 || /[^a-z .'-]+$/i.test(first_name)) {
            return "Please enter your full first name";
        } else {
            return true;
        }
    },
    
    // last_name has a minimum of 2 char
    last_name: function(last_name) {
        if (last_name.length < 2 || /[^a-z .'-]+$/i.test(last_name)) {
            return "Please enter your full last name";
        } else {
            return true;
        }
    },
    
    // phone format (just ensuring no invalid characters)
    phone: function(phone) {     
        /* Regular Expression verifies that phone has valid chars and format */
        if ((phone.length <12 && phone.length > 1)||/[^\d-. ext()]/i.test(phone)) { 
            return "Please enter a valid phone number";
        } else {
            return true;
        }
    }, 
        
    // email format
    email_address1: function(email_address1) {     
        /* 
         * Regular Expression verifies that email has valid characters before and after @ 
         * and ensures that the email ends with a period and 2 or more characters ex. abc@xyz.us
         */
        if (!(/^[a-z0-9._%+-]+@[a-z0-9-]+(\.[a-z]{2,})+$/i.test(email_address1))) { 
            return "Please enter a valid email address";
        } else {
            return true;
        }
    },

    // re-enter email filled in and that it matches first
    email_address2: function(email_address2) {     
        if (email_address2 == "") {
            return "Please re-enter your email address";
        } else if ($('#email_address1').val() !== email_address2) {
            return "Emails don't match";
        } else {
            return true;
        }
    }
};

