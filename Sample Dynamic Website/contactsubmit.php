<!DOCTYPE html>
<!-- Contact submit confirmation page -->

<html>
    <head>
        <meta name="author" content="David Tanzman">
        <meta charset="utf-8">
        <!-- set users view to 1 scale so media queries work -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Contact Form</title>
        
        <!-- Displays favicon -->
        <link rel="shortcut icon" href="images/favicon.ico">
        
        <!-- link to CSS style sheets -->
        <link rel="stylesheet" href="styles/main.css">
        <link rel="stylesheet" href="styles/slicknav.min.css">
        
        <!-- link to JavaScript files -->
        <script src="js/jquery-1.12.4.min.js"></script>
        <script src="js/jquery.slicknav.min.js"></script>
        
        <!-- call slicknav jQuery file -->
        <script type="text/javascript">
            /* when document ready pull nav items from nav_menu and add to mobile_menu*/
            $(document).ready(function() {
                $('#nav_menu').slicknav({ prependTo:"#mobile_menu" });
            });
        </script>
        
        <!-- link to import Google Web Font -->
        <link href='https://fonts.googleapis.com/css?family=Montserrat:700' rel='stylesheet' type='text/css'>
    </head>
    
    <body>
        <!-- navigation menu -->
        <nav id="mobile_menu"></nav>
        <nav id="nav_menu">
            <ul>
                <li><a href="index.html">Biography</a></li>
                <li><a href="resume.html">Resume</a></li>
                <li><a href="sample_work.html">Sample Work</a></li>
                <li><a href="contact.html" class="current">Contact</a></li>
            </ul>
	    </nav>
        <div id="content">
            <div id="content_border">
                <!-- page header -->
                <header>
                    <h1>Thank You For Submitting</h1>
                </header>

                <!--  main page content and footer -->
                <main>
                    <h2>Contact Info</h2>
                    <div class="text_box">
                        <div>
                            <strong>First Name:</strong>
                            <?php echo $_POST["first_name"]; ?>
                        </div>

                        <div>
                            <strong>Last Name: </strong>
                            <?php echo $_POST["last_name"]; ?>
                        </div>
                        
                        <div>
                            <strong>Company Name: </strong>
                            <?php echo $_POST["company_name"]; ?>
                        </div>
                        
                        <div>
                            <strong>Phone: </strong>
                            <?php echo $_POST["phone"]; ?>
                        </div>
                        
                        <div>
                            <strong>Email Address: </strong>
                            <?php echo $_POST["email_address1"]; ?>
                        </div>
                        
                        <div>
                            <strong>Message: </strong>
                            <?php echo $_POST["message"]; ?>
                        </div>
                    </div>

                    <h2>Feedback</h2>
                    <div class="text_box">
                        <div>
                            <strong>Site Rating: </strong>
                            <?php echo $_POST["rating"]; ?>
                        </div>
                        
                        <div>
                            <strong>Comments: </strong>
                            <?php echo $_POST["comments"]; ?>
                        </div>
                    </div>

                    <!-- Send contact message to personal email -->
                    <?php
 
                        if (isset($_POST['email_address1'])) {

                            // email to my servers no_reply address

                            $email_to = "no_reply@dtanzman.com";

                            function died($error) {

                                // your error code can go here

                                echo "We are very sorry, but there were error(s) found with the form you submitted. ";

                                echo "These errors appear below.<br /><br />";

                                echo $error . "<br /><br />";

                                echo "Please go back and fix these errors.<br /><br />";

                                die();
                            }

                            // validation expected data exists

                            if (!isset($_POST['first_name']) ||
                                    !isset($_POST['last_name']) ||
                                    !isset($_POST['email_address1']) ||
                                    !isset ($_POST['company_name']) ||
                                    !isset($_POST['phone']) ||
                                    !isset($_POST['message'])) {

                                died('We are sorry, but there appears to be a problem with the form you submitted.');
                            }

                            $first_name = $_POST['first_name']; // required

                            $last_name = $_POST['last_name']; // required

                            $email_from = $_POST['email_address1']; // required
                            
                            $company_name = $_POST['company_name']; // not required

                            $phone = $_POST['phone']; // not required

                            $message = $_POST['message']; // not required

                            // reset error message
                            $error_message = "";

                            // validate required fields
                            $email_exp = '/^[a-z0-9._%+-]+@[a-z0-9-]+(\.[a-z]{2,})+$/i';

                            if (!preg_match($email_exp, $email_from)) {

                                $error_message .= 'The Email Address you entered does not appear to be valid.<br />';
                            }

                            $string_exp = "/^[A-Za-z .'-]+$/";

                            if (!preg_match($string_exp, $first_name)) {

                                $error_message .= 'The First Name you entered does not appear to be valid.<br />';
                            }

                            if (!preg_match($string_exp, $last_name)) {

                                $error_message .= 'The Last Name you entered does not appear to be valid.<br />';
                            }

                            // if error_message has something in it run died() function
                            if (strlen($error_message) > 0) {

                                died($error_message);
                            }

                            $email_message = "Form details below.\n\n";

                            function clean_string($string) {

                                $bad = array("content-type", "bcc:", "to:", "cc:", "href");

                                return str_replace($bad, "", $string);
                            }

                            $email_message .= "First Name: " . clean_string($first_name) . "\n";

                            $email_message .= "Last Name: " . clean_string($last_name) . "\n";

                            $email_message .= "Email: " . clean_string($email_from) . "\n";

                            $email_message .= "Company: " . clean_string($company_name) . "\n";
                            
                            $email_message .= "Telephone: " . clean_string($phone) . "\n";

                            $email_message .= "Comments: " . clean_string($message) . "\n";
                            
                            // append email_from to email subject so know who sent
                            $email_subject = "Contact Form Message from " . clean_string($email_from);


                            // create email headers

                            $headers = 'From: ' . $email_from . "\r\n" .
                                    'Reply-To: ' . $email_from . "\r\n" .
                                    'X-Mailer: PHP/' . phpversion();

                            @mail($email_to, $email_subject, $email_message, $headers);
                            ?>

                            <!-- message indicating (to me) email sucessffully sent -->

                            <strong>Thank you for contacting me.</strong>

                        <!-- Submit form data into SQL DB using PDO to prevent SQL injection -->
                        <?php
                            $servername = "localhost";
                            // default username for XAMPP is root (diff for server)
                            $username = "dtanzman_public";
                            // default password for XAMPP is blank (diff for server)
                            $password = "test575";
                            $dbname = "dtanzman_profileContactDB";

                            try {
                                $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
                                // set the PDO error mode to exception
                                $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                                $sql = "INSERT INTO contactInfo (firstname, lastname, company, phone, email, message)
                                VALUES (:first_name, :last_name, :company_name, :phone, :email_address1, :message);
                                INSERT INTO feedback (email, rating, comments)
                                VALUES (:email_address1, :rating, :comments);";


                                // using prepared statements to prevent sql injection
                                $stmnt = $conn->prepare($sql);
                                // executing prepared statements with POSTED data
                                $stmnt->execute(
                                    array(
                                            ':first_name' => $_POST['first_name'], 
                                            ':last_name' => $_POST['last_name'],
                                            ':company_name' => $_POST['company_name'],
                                            ':phone' => $_POST['phone'],
                                            ':email_address1' => $_POST['email_address1'],
                                            ':message' => $_POST['message'],
                                            ':rating' => $_POST['rating'],
                                            ':comments'=> $_POST['comments']
                                            )
                                    );
                                // message indicating (to me) that info successfully sent to mySQL DB
                                echo "<strong>Your message has been sent successfully</strong>";
                                }
                            catch(PDOException $e)
                                {
                                echo $sql . "<br>" . $e->getMessage();
                                }

                            $conn = null;
                        ?>
                    <?php    
                    }
                    ?>
		</main>
                <footer>
                    <p>&copy; Copyright 2016 David Tanzman</p>
                </footer>
	</body>
</html> 